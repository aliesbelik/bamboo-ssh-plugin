package com.edwardawebb.atlassian.plugins.bamboo.sshplugin;

import net.schmizz.sshj.common.Base64;
import net.schmizz.sshj.common.KeyType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class SSHKeyProviderTest {
    /*
    @BeforeClass
    public static void initBouncyCastle() {
        BouncyCastleProviderUtils.getProviderName();
    }
    */

    //@Test
    public void testKeyPairReading() throws Exception {

        File initialFile = new File("src/test/resources/test_rsa.openssh");

        try {
            //InputStream is = new FileInputStream(initialFile);
            InputStream is = FileUtils.openInputStream(initialFile);
            String pk = IOUtils.toString(is);
            is.close();

            SSHKeyProvider blabla = new SSHKeyProvider(pk, "blabla");

            assertNotNull(blabla.getPrivate());
            assertNotNull(blabla.getPublic());
            assertThat(blabla.getType(), equalTo(KeyType.RSA));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void testOpenSSHKeyReading() throws Exception {

        File initialFile = new File("src/test/resources/test_rsa.openssh.nopassphrase");

        try {
            //InputStream is = new FileInputStream(initialFile);
            InputStream is = FileUtils.openInputStream(initialFile);
            String pk = IOUtils.toString(is);
            is.close();

            String privateKeyPEM = pk.replace("-----BEGIN RSA PRIVATE KEY-----\n", "")
                .replace("-----END RSA PRIVATE KEY-----", "");
            System.out.println("privateKeyPEM: " + privateKeyPEM);

            // Base64 decode the data
            byte[] encodedPrivateKey = Base64.decode(privateKeyPEM);

            // PKCS8 decode the encoded RSA private key
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            //PrivateKey privateKey = kf.generatePrivate(keySpec);
            RSAPrivateKey privateKey = (RSAPrivateKey) kf.generatePrivate(keySpec);

            System.out.println("RESULT: " + privateKey);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void testPKCS8KeyReading() throws Exception {

        // reading file in PKCS8 format
        File privateKeyFile = new File("src/test/resources/test_rsa.pkcs8.nopassphrase");

        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(privateKeyFile));
            byte[] privateKeyBytes = new byte[(int)privateKeyFile.length()];
            bis.read(privateKeyBytes);
            bis.close();

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            RSAPrivateKey privateKey = (RSAPrivateKey) kf.generatePrivate(keySpec);

            System.out.println("PrivateKeySpec: " + keySpec.toString());
            System.out.println("Algorithm: " + privateKey.getAlgorithm());
            System.out.println("RESULT: " + privateKey.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


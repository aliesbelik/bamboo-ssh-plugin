package com.edwardawebb.atlassian.plugins.bamboo.sshplugin;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;

public class BaseSshTaskConfigurator extends AbstractTaskConfigurator  {
    private static final Logger log = Logger.getLogger(BaseSshTaskConfigurator.class);

    private static final EnumSet<AuthType> SUPPORTED_AUTH_TYPES = EnumSet.of(AuthType.PASSWORD, AuthType.KEY, AuthType.KEY_WITH_PASSPHRASE);
    public static final int DEFAULT_SSH_PORT_NUMBER = 22;
    public static final int CONN_TIMEOUT = 60000;

    public static final String SCP_PLUGIN = "com.edwardawebb.atlassian.plugins.bamboo.sshplugin";
    public static final String SCP_TASK = SCP_PLUGIN + ":scptask";
    public static final String SSH_TASK = SCP_PLUGIN + ":sshtask";

    public static final String HOST = "host";
    public static final String USERNAME = "username";
    public static final String AUTH_TYPE = "authType";
    public static final String PORT = "port";
    public static final String PLAIN_PASSWORD = "password";
    public static final String PASSWORD = "encPassword";
    public static final String PRIVATE_KEY = "private_key";
    public static final String PASSPHRASE = "passphrase";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String CHANGE_KEY = "change_key";
    public static final String CHANGE_PASSPHRASE = "change_passphrase";
    public static final String TIMEOUT = "timeout";

    //private final EncryptionService encryptionService;
    private EncryptionService encryptionService;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ReverseScpTask.class);

    public BaseSshTaskConfigurator(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    /*
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }
    */

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);

        String authType = params.getString(AUTH_TYPE);
        config.put(AUTH_TYPE, AuthType.valueOf(params.getString(AUTH_TYPE)).toString());
        config.put(HOST, params.getString(HOST));
        config.put(USERNAME, params.getString(USERNAME));

        String port = params.getString(PORT);
        if (StringUtils.isNotEmpty(port)) {
            config.put(PORT, Integer.toString(NumberUtils.toInt(port, DEFAULT_SSH_PORT_NUMBER)));
        }

        String passwordChange = params.getString(CHANGE_PASSWORD);
        if ("true".equals(passwordChange)) {
            log.debug("Changing password...");
            final String password = params.getString(PLAIN_PASSWORD);
            config.put(PASSWORD, encryptionService.encrypt(password));
        } else if (previousTaskDefinition != null) {
            config.put(PASSWORD, previousTaskDefinition.getConfiguration().get(PASSWORD));
        } else {
            final String password = params.getString(PLAIN_PASSWORD);
            config.put(PASSWORD, encryptionService.encrypt(password));
        }

        String keyChange = params.getString(CHANGE_KEY);
        if ("true".equals(keyChange)) {
            String key = readPrivateKey(params);

            if (key != null) {
                log.debug("Changing private key...");
                config.put(PRIVATE_KEY, encryptionService.encrypt(key));
            }
        } else if (previousTaskDefinition != null) {
            config.put(PRIVATE_KEY,  previousTaskDefinition.getConfiguration().get(PRIVATE_KEY));
        } else {
            final String privateKey = params.getString(PRIVATE_KEY);
            config.put(PRIVATE_KEY, privateKey);
        }

        String passphraseChange = params.getString(CHANGE_PASSPHRASE);
        if ("true".equals(passphraseChange)) {
            log.debug("Changing passphrase...");
            final String passphrase = params.getString(PASSPHRASE);
            config.put(PASSPHRASE, encryptionService.encrypt(passphrase));
        } else if (previousTaskDefinition != null) {
            config.put(PASSPHRASE, previousTaskDefinition.getConfiguration().get(PASSPHRASE));
        } else {
            final String passphrase = params.getString(PASSPHRASE);
            config.put(PASSPHRASE, encryptionService.encrypt(passphrase));
        }

        if (AuthType.PASSWORD.equals(AuthType.valueOf(authType))) {
            log.debug("Using password authentication");
        } else {
            log.debug("Using private key authentication");
        }

        return config;
    }

    private String readPrivateKey(final ActionParametersMap params) {
        final File private_key_file = params.getFiles().get(PRIVATE_KEY);

        if (private_key_file != null) {
            final String key;

            try {
                key = FileUtils.readFileToString((File) private_key_file);
                return key;
            } catch (IOException e) {
                log.error("Cannot read uploaded ssh key file", e);
            }
        } else {
            log.error("Unable to load key from config submission!");
        }

        return null;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        context.put("authenticationTypes", SUPPORTED_AUTH_TYPES);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);

        context.put(HOST, taskDefinition.getConfiguration().get(HOST));

        final String port = taskDefinition.getConfiguration().get(PORT);
        if (port != null) {
            context.put(PORT, NumberUtils.toInt(port, DEFAULT_SSH_PORT_NUMBER));
        }

        context.put(USERNAME, taskDefinition.getConfiguration().get(USERNAME));
        context.put(PLAIN_PASSWORD, taskDefinition.getConfiguration().get(PLAIN_PASSWORD));

        if (StringUtils.isNotBlank(taskDefinition.getConfiguration().get(PRIVATE_KEY))) {
            context.put("private_key_defined", "true");
        }

        context.put(PASSPHRASE, taskDefinition.getConfiguration().get(PASSPHRASE));
        context.put(AUTH_TYPE, taskDefinition.getConfiguration().get(AUTH_TYPE));
        context.put("authenticationTypes", SUPPORTED_AUTH_TYPES);
        context.put("localPath", taskDefinition.getConfiguration().get("localPath"));
        context.put("remotePath", taskDefinition.getConfiguration().get("remotePath"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);

        context.put(HOST, taskDefinition.getConfiguration().get(HOST));
        context.put(USERNAME, taskDefinition.getConfiguration().get(USERNAME));
        context.put(AUTH_TYPE, taskDefinition.getConfiguration().get(AUTH_TYPE));
        context.put("localPath", taskDefinition.getConfiguration().get("localPath"));
        context.put("remotePath", taskDefinition.getConfiguration().get("remotePath"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String host = params.getString(HOST);
        if (StringUtils.isEmpty(host)) {
            errorCollection.addError(HOST, "You must specify a host to connect to");
        }

        String port = params.getString(PORT);
        if (StringUtils.isNotEmpty(port)) {
            final int portNumber = NumberUtils.toInt(port, -1);

            if (portNumber < 0) {
                errorCollection.addError(PORT, "Port number must be a positive number");
            }
        }

        String username = params.getString(USERNAME);
        if (StringUtils.isEmpty(username)) {
            errorCollection.addError(USERNAME, "You must specify a username");
        }

        AuthType authType = AuthType.valueOf(params.getString(AUTH_TYPE));
        switch (authType) {
            case PASSWORD:
                if ("true".equals(params.getString(CHANGE_PASSWORD))) {
                    String password = params.getString(PLAIN_PASSWORD);

                    if (StringUtils.isEmpty(password)) {
                        errorCollection.addError(PLAIN_PASSWORD, "You must specify password");
                    }
                }
                break;
            case KEY:
                /*
                if ("true".equals(params.getString(CHANGE_KEY))) {
                    validatePrivateKey(params, errorCollection);
                }
                */
                if (params.getBoolean(CHANGE_KEY)) {
                    String privateKey = readPrivateKey(params);

                    if (StringUtils.isBlank(privateKey)) {
                        errorCollection.addError(PRIVATE_KEY, "You must specify private key");
                    } else {
                        SSHKeyProvider.validatePrivateKey(privateKey, "", errorCollection);
                    }
                }
                break;
            case KEY_WITH_PASSPHRASE:
                /*
                if ("true".equals(params.getString(CHANGE_KEY))) {
                    validatePrivateKey(params, errorCollection);
                }
                if ("true".equals(params.getString(CHANGE_PASSPHRASE))) {
                    String passphrase = params.getString(PASSPHRASE);

                    if (StringUtils.isEmpty(passphrase)) {
                        errorCollection.addError(PASSPHRASE, "You must specify private key passphrase");
                    }
                }
                */
                if (params.getBoolean(CHANGE_KEY)) {
                    String privateKey = readPrivateKey(params);

                    if (StringUtils.isBlank(privateKey)) {
                        errorCollection.addError(PRIVATE_KEY, "You must specify private key");
                    } else {
                        SSHKeyProvider.validatePrivateKey(privateKey, params.getString(PASSPHRASE), errorCollection);
                    }
                }
                if ("true".equals(params.getString(CHANGE_PASSPHRASE))) {
                    String passphrase = params.getString(PASSPHRASE);
                    if (StringUtils.isBlank(passphrase)) {
                        errorCollection.addError(PASSPHRASE, "You must specify private key passphrase");
                    }
                }
                break;
        }
    }
}

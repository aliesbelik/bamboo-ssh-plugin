package com.edwardawebb.atlassian.plugins.bamboo.sshplugin.crypto;

import com.atlassian.security.random.DefaultSecureRandomService;
import com.atlassian.security.random.SecureRandomService;
import net.schmizz.sshj.transport.random.Random;

public class AtlassianRandom implements Random {
    private final SecureRandomService secureRandomService;

    public static class Factory implements net.schmizz.sshj.common.Factory.Named<Random> {
        @Override
        public Random create() {
            return new AtlassianRandom();
        }

        @Override
        public String getName() {
            return "atlassian";
        }
    }

    public AtlassianRandom() {
        secureRandomService = DefaultSecureRandomService.getInstance();
    }

    @Override
    public void fill(byte[] output, int start, int len) {
        final byte[] randomBytes = new byte[len];
        secureRandomService.nextBytes(randomBytes);
        System.arraycopy(randomBytes, 0, output, start, len);
    }
}
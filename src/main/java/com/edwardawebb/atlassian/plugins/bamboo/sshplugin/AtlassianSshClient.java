package com.edwardawebb.atlassian.plugins.bamboo.sshplugin;

import com.edwardawebb.atlassian.plugins.bamboo.sshplugin.crypto.AtlassianRandom;
import net.schmizz.sshj.DefaultConfig;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.random.SingletonRandomFactory;

public class AtlassianSshClient extends SSHClient {
    public AtlassianSshClient() {
        super(new AtlassianDefaultSshConfig());
    }

    private static class AtlassianDefaultSshConfig extends DefaultConfig {
        @Override
        protected void initRandomFactory(final boolean bouncyCastleRegistered) {
            setRandomFactory(new SingletonRandomFactory(new AtlassianRandom.Factory()));
        }
    }
}